import random
from time import time
from math import log

N_MIN = 10**3
N_MAX = 10**4
STEP = 10**3
TIME_MUL = 10**7
REPS = 10**2

def generate_dataset(n):
    return random.sample(range(2*n), n)


def select(k, S):
    if len(S) <= 140: return sorted(S)[k-1]
    parts = [S[i:i+5] for i in range(0, len(S), 5)]
    list(map(list.sort, parts))
    medians = list(map(lambda x: x[len(x)//2], parts))
    m = select(len(medians)//2+1, medians)
    S = [s for s in S if s < m] + [m] + [s for s in S if s > m]
    i = S.index(m) + 1
    if i == k: return m
    return select(k, S[0:i-1]) if i > k else select(k-i, S[i:])


def measure_time(f, args):	
    start = time()
    f(*args)
    return time() - start

	
def repeat(f, data_size, complexity):
    time = 0
    for i in range(REPS):
        S = generate_dataset(data_size)
        k = random.randint(1, data_size)
        time += measure_time(f, (k, S))
    return ((time * TIME_MUL) / REPS) / complexity(data_size)


for n in range(N_MIN, N_MAX+STEP, STEP):
    print("n = {:5d}\t{:.2f}".format(n, repeat(select, n, lambda n: n)))
