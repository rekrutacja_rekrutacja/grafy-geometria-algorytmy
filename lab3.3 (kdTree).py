import collections
import random
# from math import inf
inf = float('inf')

Point = collections.namedtuple('Point', ['x', 'y'])


class Node:
    def __init__(self, value, boundaries):
        self.left = None
        self.right = None
        self.value = value
        self.boundaries = boundaries


class Boundaries:
    def __init__(self, left, top, right, bottom):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom

    def isIn(self, other):
        if (self.top <= other.top and
            self.bottom >= other.bottom and
            self.left >= other.left and
            self.right <= other.right):
            return True
        else:
            return False

    def intersects(self, other):
        if (self.top >= other.top >= other.bottom or
            self.top >= other.bottom >= self.bottom or
            self.left <= other.left <= self.right or
            self.left <= other.right <= self.right):
            return True
        else:
            return False

    def contains(self, point):
        if (self.top >= point.y >= self.bottom and
            self.left <= point.x <= self.right):
            return True
        else:
            return False


def median(S):
    l = len(S)
    if l % 2 == 0:
        k = len(S) // 2
    else:
        k = len(S) // 2 + 1
    if len(S) <= 140: return sorted(S)[k - 1]
    parts = [S[i:i + 5] for i in range(0, len(S), 5)]
    list(map(list.sort, parts))
    medians = list(map(lambda x: x[len(x) // 2], parts))
    m = median(len(medians) // 2 + 1, medians)
    S = [s for s in S if s < m] + [m] + [s for s in S if s > m]
    i = S.index(m) + 1
    if i == k: return m
    return median(k, S[0:i - 1]) if i > k else median(k - i, S[i:])


# def generate_points(n):
#     xs = random.sample(range(1, 10 * n), n)
#     ys = random.sample(range(1, 10 * n), n)
#     return [Point(x, y) for x, y in zip(xs, ys)]


def buildKDTree(points, boundaries, depth):
    if len(points) == 1:
        return Node(points[0], None)
    elif depth % 2 == 0:
        m = median([p.x for p in points])
        S1 = [p for p in points if p.x <= m]
        S2 = [p for p in points if p.x > m]

        boundaries1 = Boundaries(boundaries.left, boundaries.top, m, boundaries.bottom)
        left = min(x for x in [p.x for p in S2] if x > m)
        boundaries2 = Boundaries(m, boundaries.top, boundaries.right, boundaries.bottom)
    else:
        m = median([p.y for p in points])
        S1 = [p for p in points if p.y <= m]
        S2 = [p for p in points if p.y > m]

        boundaries1 = Boundaries(boundaries.left, m, boundaries.right, boundaries.bottom)
        bottom = min(y for y in [p.y for p in S2] if y > m)
        boundaries2 = Boundaries(boundaries.left, boundaries.top, boundaries.right, m)

    v = Node(None, boundaries)
    v.left = buildKDTree(S1, boundaries1, depth + 1)
    v.right = buildKDTree(S2, boundaries2, depth + 1)
    return v


def kdTreeQuery(v, R):
    l = []
    if v.value is not None:
        return [v.value] if R.contains(v.value) else [None]
    else:
        if v.left.boundaries is not None:
            if v.left.boundaries.isIn(R):
                l += reportSubtree(v.left)
            elif v.left.boundaries.intersects(R):
                l += kdTreeQuery(v.left, R)
        else:
            l += [v.left.value] if R.contains(v.left.value) else []
        if v.right.boundaries is not None:
            if v.right.boundaries.isIn(R):
                l += reportSubtree(v.right)
            elif v.right.boundaries.intersects(R):
                l += kdTreeQuery(v.right, R)
        else:
            l += [v.right.value] if R.contains(v.right.value) else []
    return l


def reportSubtree(v):
    points = []
    walk(v, points)
    return points


def walk(node, l):
    if node is not None:
        walk(node.left, l)
        if node.value is not None:
            l.append(node.value)
        walk(node.right, l)


def write(points):
    points.sort(key=lambda p: p.x)
    print('Obszar zawiera: ', end='')
    for p in points:
        print('({}, {})'.format(p.x, p.y), end=' ')
    print('\n')


if __name__ == '__main__':
    points = [Point( 1, 11), Point( 2, 3), Point( 3,  8), Point( 4, 2),
              Point( 5,  7), Point( 7, 5), Point( 8, 10), Point( 9, 1),
              Point(10,  6), Point(12, 4), Point(13,  0), Point(14, 9)]

    random.shuffle(points)
    v = buildKDTree(points, Boundaries(-inf, inf, inf, -inf), 0)

    # p = kdTreeQuery(v, Boundaries(2, 9, 6, 6))
    # p = kdTreeQuery(v, Boundaries(3, 8, 12, 2))
    # p = kdTreeQuery(v, Boundaries(-inf, inf, inf, -inf))
    # write(p)

    while True:
        a = list(map(int, input('Podaj lewy górny róg (x, y): ').split(',')))
        b = list(map(int, input('Podaj prawy dolny róg (x, y): ').split(',')))
        # write(kdTreeQuery(v, Boundaries(*a, *b)))
        write(kdTreeQuery(v, Boundaries(a[0], a[1], b[0], b[1])))
        
