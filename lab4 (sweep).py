import collections

Point = collections.namedtuple('Point', ['x', 'y'])
Segment = collections.namedtuple('Segment', ['beg', 'end'])

class Stack:
     def __init__(self):
         self.items = []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[-1]
     
     def is_empty(self):
         return self.items == []

     def __repr__(self):
          return str(self.items)


def sign(x):
    return 1 if x >= 0 else -1


def det(p1, p2, p3):
    return p1.x*p2.y + p2.x*p3.y + p3.x*p1.y - p3.x*p2.y - p1.x*p3.y - p2.x*p1.y


def  is_collinear(p1, p2, p3):
    return det(p1, p2, p3) == 0


def overlaps(s1, s2):
     if is_collinear(s1.beg, s1.end, s2.beg) and is_collinear(s1.beg, s1.end, s2.end):
          return (min(s1.beg.x, s1.end.x) < s2.beg.x < max(s1.beg.x, s1.end.x)
                  or min(s1.beg.x, s1.end.x)< s2.end.x < max(s1.beg.x, s1.end.x)
                  or min(s2.beg.x, s2.end.x)< s1.beg.x < max(s2.beg.x, s2.end.x)
                  or min(s2.beg.x, s2.end.x)< s1.end.x < max(s2.beg.x, s2.end.x)
                  or ((s1.beg == s2.beg and s1.end == s2.end) or (s1.beg == s2.end and s1.end == s2.beg)))
     return False


def segments_intersect(s1, s2):
     if overlaps(s1, s2):
          return True
     sign1 = sign(det(s1.beg, s1.end, s2.beg))
     sign2 = sign(det(s1.beg, s1.end, s2.end))
     sign3 = sign(det(s2.beg, s2.end, s1.beg))
     sign4 = sign(det(s2.beg, s2.end, s1.end))
     if sign1 != sign2 and sign3 != sign4:
          return True
     return False


def intersects(segment, S):
     for s in S:
          if segments_intersect(s, segment):
               return True
     return False


def sweep_triangulation(points, cycle, top, bottom):
     diagonals = []
     edges = [Segment(beg, end) for beg, end in zip(cycle[:-1], cycle[1:])]
     n = len(points)
     v = [None] + points
     stack = Stack()
     stack.push(v[1])
     stack.push(v[2])
     for i in range(3,n):
          print(stack)
          if (v[i] in bottom and stack.peek() not in bottom
              or v[i] in top and stack.peek() not in top):
               for w in stack.items[1:]:
                    diagonals.append(Segment(v[i], w))
                    edges.append(Segment(v[i], w))
                    wk = stack.pop()
                    stack = Stack()
                    stack.push(wk)
                    stack.push(v[i])
          else:
               last = stack.pop()
               while (not stack.is_empty()
                      and not intersects(Segment(v[i], stack.peek()), edges)):
                    last = stack.pop()
                    diagonals.append(Segment(v[i], last))
                    edges.append(Segment(v[i], w))
               stack.push(last)
               stack.push(v[i])
             
     for w in stack.items[1:-1]:
          diagonals.append(Segment(v[n], w))
     return diagonals


points = [Point(*list(map(int, line.rstrip('\n').split(','))))
          for line in open('./fig.txt')]

pivot = points.index(max(points, key=lambda p: p.x))
cycle = points + [points[0]]
top = points[pivot:]
bottom = points[:pivot]

points = sorted(points, key=lambda p: p.x)
diagonals = sweep_triangulation(points, cycle, top, bottom)

for d in diagonals:
     s1 = '({}, {})'.format(d.beg.x, d.beg.y)
     s2 = '({}, {})'.format(d.end.x, d.end.y)
     l = 19-len(s1+s2)
     print(s1, '-'*l, s2)
