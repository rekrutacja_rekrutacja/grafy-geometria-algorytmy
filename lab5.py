import collections
from math import sqrt
from random import sample
from itertools import chain, combinations
from time import time
from math import log2

Point = collections.namedtuple('Point', ['x', 'y'])

POINTS = [Point(i, j) for i in range(32) for j in range(32)]
N_MIN = 5
N_MAX = 12
STEP = 1
TIME_MUL = 10**12

def power_set(iterable):
    elements = list(iterable)
    return list(chain.from_iterable(combinations(elements, n) for n in range(len(elements) + 1)))


def distance(p1, p2):
    return sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)


def generate_points(amount):
    return sample(POINTS, amount)


def write(arr):
    print()
    for row in arr:
        print(list(map(lambda x: round(x, 2) if x is not None else None, row)))
    print()


def shortest_hamiltonian_cycle(A, points, subsets):
    x = 0
    n = len(points)
    for i in range(2 ** n):
        x+=1
        for j in range(n):
            x+=1
            S = subsets[i]
            if len(S) == 0:
                A[i][j] = [distance(points[0], points[j]), [points[j]]]
            else:
                x+=1
                a = []
                for d in range(len(S)):
                    x+=1
                    deleted = S[d]
                    row = subsets.index(S[:d] + S[d + 1:])
                    col = points.index(deleted)
                    a.append([A[row][col][0] + distance(deleted, points[j]), A[row][col][1] + [points[j]]])
                A[i][j] = min(a)
    print('x=', x)
    ans = []
    for i in range(1, len(points)):
        x = tuple(points[1:i] + points[i + 1:])
        idx = subsets.index(x)
        ans.append([A[idx][i][0] + distance(points[i], points[0]), A[idx][i][1]])
    return min(ans, key=lambda x: x[0])


def measure_time(f, args):
    start = time()
    f(*args)
    return time() - start


def repeat(f, data_size, complexity):
    A = [[None for i in range(n)] for j in range(2 ** n)]
    points = generate_points(data_size)
    subsets = power_set(points)
    time = measure_time(f, (A, points, subsets))
    return (time * TIME_MUL) / complexity(data_size)





if __name__ == '__main__':
    for n in range(N_MIN, N_MAX + STEP, STEP):
        print("n = {:5d}\t{:.2f}".format(n, repeat(shortest_hamiltonian_cycle, n, lambda n: n*n * n**4 * 2**n)))

    # points = [Point(1, 6), Point(3, 3), Point(4, 1), Point(4, 5), Point(6, 2), Point(9, 5)]
    # subsets = power_set(points)
    # n = len(points)
    # A = [[None for i in range(n)] for j in range(2 ** n)]
    #
    # ans = shortest_hamiltonian_cycle(A, points, subsets)
    #
    # print('length =', ans[0])
    # print('cycle: ({}, {})'.format(points[0].x, points[0].y), end=' -> ')
    # for i in range(len(ans[1])):
    #     print('({}, {})'.format(ans[1][i].x, ans[1][i].y), end=' -> ')
    # print('({}, {})'.format(points[0].x, points[0].y))
