from math import sqrt
import random
from time import time
import matplotlib.pyplot as plt
import numpy as np


class Point:
    def __init__(self, x, y, dist=0):
        self.x = x
        self.y = y
        self.dist = dist

    def __repr__(self):
        return '({}, {})'.format(self.x, self.y)

    def details(self):
        return self.__repr__() + '  : |AB| = {}'.format(self.dist)


N_MIN = 10 ** 2
N_MAX = 10 ** 3
STEP = 10 ** 2
TIME_MUL = 10 ** 7
REPS = 10


def draw_plot(points, hospitals):
    n = len(points)
    plt.scatter([p.x for p in points], [p.y for p in points], c='black', s=50)
    plt.xticks(np.arange(0, 3 * n + 1, n / 10))
    plt.yticks(np.arange(0, 3 * n + 1, n / 10))
    plt.scatter([h.x for h in hospitals], [h.y for h in hospitals], s=200)
    plt.grid(True)
    plt.show()


def distance(p1, p2):
    return sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)


def generate_points(n):
    xs = random.sample(range(3 * n), n)
    ys = random.sample(range(3 * n), n)
    return [Point(x, y) for x, y in zip(xs, ys)]


def furthest_first(points, k):
    hospitals = [random.choice(points)]
    for p in points:
        p.dist = distance(p, hospitals[0])
    for i in range(2, k + 1):
        hospitals.append(max(points, key=lambda p: p.dist))
        for p in points:
            dist = distance(p, hospitals[-1])
            if dist < p.dist:
                p.dist = dist
    return hospitals


def measure_time(f, args):
    start = time()
    f(*args)
    return time() - start


def repeat(function, number_of_points, number_of_hospitals, complexity):
    time = 0
    for i in range(REPS):
        points = generate_points(number_of_points)
        time += measure_time(function, (points, number_of_hospitals))
    return ((time * TIME_MUL) / REPS) / complexity(number_of_points, number_of_hospitals)


if __name__ == '__main__':

    # points = generate_points(100)
    # hospitals = furthest_first(points, 5)
    # draw_plot(points, hospitals)

    for n in range(N_MIN, N_MAX + STEP, STEP):
        k = random.randint(3, n)
        print("n = {:4d}\t{:.2f}".format(n, repeat(furthest_first, n, k, lambda n, k: n*k)))
